Pod::Spec.new do |s|  
    s.name                    = 'HYLog'
    s.version                 = '1.0.5'
    s.summary                 = 'HYLog is an OS level network enabled logging framework for iOS.'
    s.homepage                = 'https://www.heady.io'

    s.author                  = { 'HYLog' => 'tirupati@heady.io' }
    s.license                 = { :type => 'MIT', :file => 'LICENSE' }

    s.platform                = :ios
    s.source                  = { :git => 'https://bitbucket.org/headyio/hylog.git', :branch => "master" }
    s.source_files            = "Sources/*.{swift}"
    s.ios.deployment_target   = '10.0'
    s.swift_versions          = '5.0'
end 
