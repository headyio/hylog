//
//  ViewController.swift
//  HYLogDemo
//
//  Created by Tirupati Balan on 04/10/20.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        HYLog.shared.isNetworkEnabled = true

        //Trace with default viewlifecycle category
        HYLog.trace()
    }

    @IBAction func showLog() {
        //1
        HYLog.trace("**** Trace 1 with default data category")

        //2
        HYLog.info("**** Info 2 here with default data Category")
        
        //3
        HYLog.info("**** Info here with viewLifeCycle Category", .viewLifeCycle)

        //4
        HYLog.error("Error here with default data Category")
        
        //5
        HYLog.error("Error here with data Category", .data)

        //6
        HYLog.warning("Warning here with default data Category")
        
        //7
        HYLog.warning("Warning here with data Category", .stats)

        //8
        HYLog.log(.warning, "Log 3 here with level & category as parameter", .data)

    }

}

