//
//  NetworkSessionManager.swift
//  HYLogDemo
//
//  Created by Tirupati Balan on 30/10/20.
//

import Foundation

extension Data {
    /// Decode data to model
    func dataToModel<T: Decodable>(_ model: T.Type) throws -> T? {
        return try JSONDecoder().decode(model, from: self)
    }
    
    /// Convert data to json object
    func dataToJsonObject(forKey key: String) -> Any? {
        if let json = try? JSONSerialization.jsonObject(with: self, options: []) as? [String: Any] {
            return json[key]
        }
        return nil
    }
}

/// Protocol to get localizedDescription of the error
protocol LocalizedDescriptionError: Error {
    /// Get localized description of the error
    var localizedDescription: String { get }
}

/// Custom error handler based on response
enum CustomError: LocalizedDescriptionError {
    case invalidArray(model: String)
    case invalidDictionary(model: String)
    case invalidData
    case customError(message: String)
    
    var localizedDescription: String {
        switch self {
            case .invalidArray(model: let model):
                return "\(model) has an invalid array"
            case .invalidDictionary(model: let model):
                return "\(model) has an invalid dictionary"
            case .invalidData:
                return "Invalid data"
            case .customError(message: let message):
                return "\(message)"
        }
    }
}

/// To send data to server using URLSession
class NetworkSessionManager: URLSession {
    
    class var defaultSharedInstance: URLSession {
        struct defaultSingleton {
            static let instance = URLSession(configuration: .default)
        }
        return defaultSingleton.instance
    }
    
    class var ephemeralSharedInstance: URLSession {
        struct ephemeralSingleton {
            static let instance = URLSession(configuration: .ephemeral)
        }
        return ephemeralSingleton.instance
    }
    
    class var backgroundSharedInstance: URLSession {
        struct backgroundSingleton {
            static let instance = URLSession(configuration: URLSessionConfiguration.background(withIdentifier: "com.heady.hylog"))
        }
        return backgroundSingleton.instance
    }
    
    class func requestWithMethod(_ method: APIRequestMethod, urlString: String, params: [String: Any]?, success: ((Data) -> Void)? = nil, failure: ((CustomError) -> Void)? = nil) {
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        
        request.httpMethod = method.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("ios", forHTTPHeaderField: "Client-Id")
        request.setValue("Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoidG9kb191c2VyIn0.A-FVHlakLbtId9jZzxbAod92epYoAJl9JWtwgFayYOI", forHTTPHeaderField: "Authorization")
                
        if let params = params, let httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])  {
            request.httpBody = httpBody
        }
        
        NetworkSessionManager.defaultSharedInstance.dataTask(with: request, completionHandler: { (data, response, error) in
            self.responseHandle(data: data, response: response, error: error, success: success, failure: failure)
            }).resume()
    }
        
    class func responseHandle(data: Data?, response: URLResponse?, error: Error?, success: ((Data) ->
        Void)? = nil, failure:((CustomError) -> Void)? = nil) {
        if let error = error {
            failure?(CustomError.customError(message: error.localizedDescription))
            return
        }
        
        guard let response = response as? HTTPURLResponse else {
            let error = CustomError.customError(message: "Invalid response")
            failure?(error)
            return
        }
        
        guard let data = data else {
            let error = CustomError.customError(message: "Invalid data")
            failure?(error)
            return
        }
        
        switch (response.statusCode) {
            case 200...299:
                success?(data)
                break
            default:
                let error = CustomError.customError(message: "Error with response code \(response.statusCode)")
                failure?(error)
        }
    }
}
