//
//  OSLog+Extension.swift
//  HYLogDemo
//
//  Created by Tirupati Balan on 04/10/20.
//

import Foundation
import os.log

extension OSLog {
    /// OSLog subsystem with default bundleIdentifier
    private static var subsystem = Bundle.main.bundleIdentifier!
    
    /// Convenience init with category
    convenience init(category: String) {
        self.init(subsystem: OSLog.subsystem, category: category)
    }
    
    // MARK - User defined function
    
    static func log(message: String, category: String, type: OSLogType) {
        os_log("%s", log: OSLog(category: category), type: type, message)
    }
    
}
