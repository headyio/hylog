//
//  HYLogLevel.swift
//  HYLogDemo
//
//  Created by Tirupati Balan on 05/10/20.
//

import Foundation
import os.log

/// HYLogLevel to print log based on defined level
public enum HYLogLevel: String, Codable, CaseIterable {
    /// Appropriate for messages that contain information normally of use only when
    /// tracing the execution of a program.
    case trace

    /// Appropriate for messages that contain information normally of use only when
    /// debugging a program.
    case debug

    /// Appropriate for informational messages.
    case info

    /// Appropriate for conditions that are not error conditions, but that may require
    /// special handling.
    case notice

    /// Appropriate for messages that are not error conditions, but more severe than
    /// `.notice`.
    case warning

    /// Appropriate for error conditions.
    case error

    /// Appropriate for error conditions.
    case fault

    /// Appropriate for critical error conditions that usually require immediate
    /// attention.
    case critical
            
    /// Matching `Logger` log type with existing `os_log` log type
    func getOSLogType() -> OSLogType {
        switch self {
         case .debug:
            return .debug
         case .info:
            return .info
         case .error:
            return .error
         case .fault:
            return .fault
         default:
            return .default
        }
    }
    
}
