//
//  HYLog.swift
//  HYLogDemo
//
//  Created by Tirupati Balan on 04/10/20.
//

import Foundation
import os.log

public final class HYLog {
    /// To send data on server serialwise
    let serialQueue = DispatchQueue(label: "com.queue.serial")
    
    /// Enable network to send data on server
    public var isNetworkEnabled: Bool = false
    
    /// To instantiate only one instance of HYLog
    public static let shared = HYLog()

    /// Trace function to print log based on default category .data and log level .trace
    public class func trace(_ category: HYLogCategory? = .data,
                         _ path: String = #file,
                     _ function: String = #function,
                         _ line: UInt = #line) {
        HYLog.logMessage(level: .trace, message: String(), category: category!, path, function, line)
    }
    
    /// Trace function to print log based on default category .data, log level .trace and message
    public class func trace(_ message: @escaping @autoclosure () -> Any,
                    _ category: HYLogCategory? = .data,
                        _ path: String = #file,
                    _ function: String = #function,
                        _ line: UInt = #line) {
        HYLog.logMessage(level: .trace, message: message, category: category!, path, function, line)
    }
    
    /// Info function to print log based on default category .data, log level .info and message
    public class func info(_ message: @escaping @autoclosure () -> Any,
                   _ category: HYLogCategory? = .data,
                       _ path: String = #file,
                   _ function: String = #function,
                       _ line: UInt = #line) {
        HYLog.logMessage(level: .info, message: message, category: category!, path, function, line)
    }
    
    /// Error function to print log based on default category .data, log level .error and message
    public class func error(_ message: @escaping @autoclosure () -> Any,
                    _ category: HYLogCategory? = .data,
                        _ path: String = #file,
                    _ function: String = #function,
                        _ line: UInt = #line) {
        HYLog.logMessage(level: .error, message: message, category: category!, path, function, line)
    }

    /// Debug function to print log based on default category .data,  log level .debug and message
    public class func debug(_ message: @escaping @autoclosure () -> Any,
                    _ category: HYLogCategory? = .data,
                        _ path: String = #file,
                    _ function: String = #function,
                        _ line: UInt = #line) {
        HYLog.logMessage(level: .debug, message: message, category: category!, path, function, line)
    }
    
    /// Warning function to print log based on default category .data, log level .warning and message
    public class func warning(_ message: @escaping @autoclosure () -> Any,
                      _ category: HYLogCategory? = .data,
                          _ path: String = #file,
                      _ function: String = #function,
                          _ line: UInt = #line) {
        HYLog.logMessage(level: .warning, message: message, category: category!, path, function, line)
    }
    
    /// Log function to print log based on default category .data and log level .trace
    public class func log(_ level: HYLogLevel = .trace,
                 _ message: @escaping @autoclosure () -> Any,
                _ category: HYLogCategory? = .data,
                    _ path: String = #file,
                _ function: String = #function,
                    _ line: UInt = #line) {
        HYLog.logMessage(level: level, message: message, category: category!, path, function, line)
    }
    
    /// Private log function to print it based on level, message, category, file, function and line
    class private func logMessage(level: HYLogLevel = .trace,
                                message: @escaping @autoclosure () -> Any,
                               category: HYLogCategory = .data,
                                 _ path: String,
                             _ function: String,
                                 _ line: UInt) {
        HYLog.shared._log(level,
                          message(),
                          category,
                          file: path,
                          function: function,
                          line: line)
    }
    
}

extension HYLog {
    /// Shared log function to print it based on level, message, category, file, function and line
    private func _log(_ level: HYLogLevel,
                    _ message: @escaping @autoclosure () -> Any,
                   _ category: HYLogCategory,
                         file: String,
                     function: String,
                         line: UInt) {
        let _message = String(describing: message())
        let _class = file.components(separatedBy: "/").last ?? file
        let _logMessage = "\(_message.isEmpty ? String() : "\(_message) ")Class: \(_class) Function: \(function) Line: \(line)"
        
        if #available(iOS 14.0, *) {
            let logger = Logger(category)
            switch level {
             case .trace:
                logger.trace("\(_logMessage)")
             case .debug:
                logger.debug("\(_logMessage)")
             case .info:
                logger.info("🟢\(_logMessage)")
             case .notice:
                logger.notice("\(_logMessage)")
             case .warning:
                logger.warning("🟡\(_logMessage)")
             case .error:
                logger.error("🔴\(_logMessage)")
             case .critical:
                logger.critical("\(_logMessage)")
             case .fault:
                logger.fault("\(_logMessage)")
            }
        } else {
            OSLog.log(message: _logMessage, category: category.rawValue, type: level.getOSLogType())
        }
                
        guard let infoDictionary = Bundle.main.infoDictionary, let displayName = infoDictionary["CFBundleName"] as? String else {
            return
        }

        let postData: [String: Any] = ["message": _message, "class": _class, "method": function, "line": line, "project_name": displayName, "bundle_id": Bundle.main.bundleIdentifier ?? String()]
        postDataOnCloud(postData)
    }
}

private extension HYLog {

    /// To post data on our cloud server
    func postDataOnCloud(_ data: [String: Any]?) {
        guard isNetworkEnabled, let data = data else {
            return
        }

        serialQueue.async {
            NetworkSessionManager.requestWithMethod(.post, urlString: NetworkConfiguration.api(.hylog), params: data)
        }
    }
}

extension Bundle {
    var displayName: String? {
        return Bundle.main.infoDictionary!["CFBundleName"] as? String
    }
}
