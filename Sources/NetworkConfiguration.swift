//
//  NetworkConfiguration.swift
//  HYLogDemo
//
//  Created by Tirupati Balan on 30/10/20.
//

import Foundation

enum APIRequestMethod: String {
    case get
    case post
    case put
    case delete
}

enum APIRequestType {
    case hylog

    func urlString() -> String {
        return NetworkConfiguration.api(self)
    }
    
    fileprivate func apiString() -> String {
        switch self {
        case .hylog:
            return "hylog"
        }
    }
    
    func responseKey() -> String {
        switch self {
        case .hylog:
            return "data"
        }
    }
}

struct NetworkConfiguration {
    
    fileprivate static let DevBaseUrl = "http://hylog.heady.io:3001/"
    fileprivate static let ProductionBaseUrl = "http://hylog.heady.io:3001/"
    fileprivate static let apiPath = String()
    fileprivate static let apiVersion = String()

    static var baseUrl: String = {
        #if DEBUG
        return DevBaseUrl
        #else
        return ProductionBaseUrl
        #endif
    }()
    
    static func api(_ apiType: APIRequestType) -> String {
        return baseUrl + apiPath + apiVersion + apiType.apiString()
    }
            
}
