//
//  Logger+Extension.swift
//  HYLogDemo
//
//  Created by Tirupati Balan on 04/10/20.
//

import Foundation
import os.log

@available(iOS 14.0, *)
extension Logger {
    /// OSLog subsystem with default bundleIdentifier
    private static var bundleIdentifier = Bundle.main.bundleIdentifier!

    /// Convenience init with category
    init(_ category: HYLogCategory?) {
        //Initalizer matches with OSLog
        self.init(subsystem: Logger.bundleIdentifier, category: category?.rawValue ?? HYLogCategory.data.rawValue)
    }
    
}
