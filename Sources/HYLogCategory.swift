//
//  HYLogCategory.swift
//  HYLogDemo
//
//  Created by Tirupati Balan on 05/10/20.
//

import Foundation

/// Log category with different log type
public enum HYLogCategory: String {
    /// Appropriate to log view life cycle methods
    case viewLifeCycle
    
    /// Appropriate to log all common data
    case data
    
    /// Appropriate to log all stats in the app
    case stats
}
